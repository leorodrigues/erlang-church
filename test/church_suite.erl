%%%-------------------------------------------------------------------
%%% @author Leonardo R. Teixeira
%%% @copyright (C) 2021, leonardo.org
%%% @doc
%%%
%%% @end
%%% Created : 12. Sep 2021 8:46 AM
%%%-------------------------------------------------------------------
-module(church_suite).
-author("Leonardo R. Teixeira").

-include_lib("common_test/include/ct.hrl").

-export([all/0]).

%% API
-export([
  case_1/1, case_2/1, case_3/1, case_4/1, case_5/1, case_6/1, case_7/1, case_8/1
]).

-import(church, [
  chain_apply/2, partial_left/2, partial_right/2, break_on_error/1
]).

case_1(_) ->
  ct:comment("Should just return the same input if the list is empty"),
  112358 = chain_apply(112358, [ ]).

case_2(_) ->
  ct:comment("Should apply all functions in the list"),
  224718 = chain_apply(112358, [
    fun(N) -> N + 1 end,
    fun(N) -> N * 2 end
  ]).

case_3(_) ->
  ct:comment("Should break before reaching the last function in the list"),
  112359 = chain_apply(112358, [
    fun(N) -> N + 1 end,
    fun(N) -> {break, N} end,
    fun(N) -> N * 2 end
  ]).

case_4(_) ->
  ct:comment("Should partialy apply to the left"),
  Sum = fun (A, B) -> A + B end,
  F = partial_left(Sum, [5]),
  11 = F(6).

case_5(_) ->
  ct:comment("Should chain applications"),
  Sum = fun (A, B) -> A + B end,
  Mul = fun (A, B) -> A * B end,
  AddFive = partial_left(Sum, [5]),
  MulTwo = partial_left(Mul, [2]),
  12 = chain_apply(1, [AddFive, MulTwo]).

case_6(_) ->
  ct:comment("Should partially apply to the right"),
  Tuplify = fun (A, B, C) -> {A, B, C} end,
  F = partial_right(Tuplify, [1, 2]),
  {1, 2, 3} = F(3).

case_7(_) ->
  ct:comment("Should wrap the input in a break tuple"),
  {break, {error, "fake reason"}} = break_on_error({error, "fake reason"}).

case_8(_) ->
  ct:comment("Should pass the input forward unaltered"),
  "not an error" = break_on_error("not an error").

all() -> [case_1, case_2, case_3, case_4, case_5, case_6, case_7, case_8].
