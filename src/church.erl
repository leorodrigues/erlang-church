-module(church).
-author("Leonardo R Teixeira").

-export([chain_apply/2, partial_left/2, partial_right/2, break_on_error/1]).

-type func() :: fun() | reference().

%%------------------------------------------------------------------------------
%% @doc Will wrap the input in a break tuple if the input is a tuple of the
%% form {error, term()}.
%% @end
%%------------------------------------------------------------------------------
-spec(break_on_error(Input :: term()) -> {break, term()} | term()).
break_on_error({error, _} = Input) -> {break, Input};
break_on_error(Input) -> Input.

%%------------------------------------------------------------------------------
%% @doc Will chain the functions applying from left to right feeding the return
%% of the current function into the argument list of the next one.
%%
%% It will stop the process if one of the functions returns the tuple
%% {:break, Result}.
%% @end
%%------------------------------------------------------------------------------
-spec(chain_apply(Value :: term(), Functions :: [func()]) -> term()).
chain_apply({break, Value}, _) -> Value;
chain_apply(Value, [ ]) -> Value;
chain_apply(Value, [Current|Remaining]) ->
  chain_apply(Current(Value), Remaining).

%%------------------------------------------------------------------------------
%% @doc Fixes the parameters of a function EXCEPT FOR THE LEFTMOST which
%% will be the sole parameter of the resulting function.
%% @end
%%------------------------------------------------------------------------------
-spec(partial_left(Function :: func(), Arguments :: [term()]) -> fun()).
partial_left(Function, Arguments) ->
  fun (Input) -> apply(Function, [Input|Arguments]) end.

%%------------------------------------------------------------------------------
%% @doc Fixes the parameters of a function EXCEPT FOR THE RIGHTMOST which
%% will be the sole parameter of the resulting function.
%% @end
%%------------------------------------------------------------------------------
-spec(partial_right(Function :: func(), Arguments :: [term()]) -> fun()).
partial_right(Function, Arguments) ->
  fun (Input) -> apply(Function, append_to_list(Arguments, Input)) end.

append_to_list(List, Element) ->
  move_elements(lists:reverse(List), [Element]).

move_elements([ ], List) -> List;
move_elements([Head|Tail], List) ->
  move_elements(Tail, [Head|List]).
